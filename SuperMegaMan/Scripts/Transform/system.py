import sys

# IronPython standard lib:
sys.path.append("C:/Program Files (x86)/IronPython 2.7/Lib")
# MonoGame
sys.path.append("C:/Program Files (x86)/MonoGame/v3.0/Assemblies/Windows")
# SuperMegaMan directory (for Component Engine)
sys.path.append("D:/Magic/C#/SuperMegaMan/SuperMegaMan")

# Other python directories.
sys.path.append("D:/Magic/C#/SuperMegaMan/SuperMegaMan/Scripts/Components")
sys.path.append("D:/Magic/C#/SuperMegaMan/SuperMegaMan/Scripts/Config")
sys.path.append("D:/Magic/C#/SuperMegaMan/SuperMegaMan/Scripts/Systems")

# Now we actually import all this stuff.
import clr
import os

clr.AddReference("SuperMegaMan")
clr.AddReference("MonoGame.Framework.dll")
clr.AddReference("component-engine3.0.dll")

from SuperMegaMan 		   		  	  import *

from Microsoft.Xna.Framework 		  import *
from Microsoft.Xna.Framework.Graphics import *
from Microsoft.Xna.Framework.Input    import *

from component_engine3 import *
