﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using SharpECS;

using SuperMegaMan.Components;
using SuperMegaMan.Systems;
using SuperMegaMan.WorldSystems;

namespace SuperMegaMan
{
    public class Game 
        : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager _graphics;
        SpriteBatch _spriteBatch;

        public KeyboardState _keyboard;
        public KeyboardState _prevKeyboard;

        public MouseState _mouseState;
        public MouseState _prevMouseState;

        public EntityWorld _entityWorld;

        public EntityConstructors _entityConstructors;

        public BaseEntity _playerEntity;
        public BaseEntity _enemyEntity;
        public BaseEntity _networkedPlayer;

        public GraphicsSystem _graphicsSystem;
        public BorderCollisionSystem _borderSystem;
        public CollisionSystem _collisionSystem;
        public TransformSystem _transformSystem;
        public ControllerSystem _controllerSystem;

        public TimeoutSystem _timeOutSystem;
        public NetworkSystem _networkSystem;

        public SpriteFont _consolasFont;

        bool isLoading = false;

        public Rectangle _groundRectangle;

        public Game()
        {
            _graphics = new GraphicsDeviceManager(this);
            _graphics.PreferredBackBufferWidth = 640;
            _graphics.PreferredBackBufferHeight = 480;

            IsMouseVisible = true;

            Window.AllowUserResizing = false;
            Window.AllowAltF4 = false;
            Window.Title = "Super Mega Man";

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            _entityWorld = new EntityWorld();
            _entityConstructors = new EntityConstructors(_entityWorld);

            //_enemyEntity = _entityConstructors.CreateEnemy(this);
            _playerEntity = _entityConstructors.CreatePlayer(this);
            _networkedPlayer = _entityConstructors.CreateNetworkedPlayer(this);

            _borderSystem = new BorderCollisionSystem(_entityWorld);
            _graphicsSystem = new GraphicsSystem(_entityWorld);
            _collisionSystem = new CollisionSystem(_entityWorld);
            _transformSystem = new TransformSystem(_entityWorld);
            _controllerSystem = new ControllerSystem(_entityWorld);

            _timeOutSystem = new TimeoutSystem(this);
            _networkSystem = new NetworkSystem();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            isLoading = true;

            _consolasFont = Content.Load<SpriteFont>("Consolas");
            _playerEntity.FindComponent<GraphicsComponent>().Texture = Content.Load<Texture2D>("Player");
            _networkedPlayer.FindComponent<GraphicsComponent>().Texture = Content.Load<Texture2D>("Player");
            //_enemyEntity.FindComponent<GraphicsComponent>().Texture = Content.Load<Texture2D>("Player");

            isLoading = false;
        }

        protected override void UnloadContent() { }

        protected override void Update(GameTime gameTime)
        {
            _keyboard = Keyboard.GetState();
            _mouseState = Mouse.GetState();

            _controllerSystem.Update(gameTime);
            _transformSystem.Update(gameTime);
            _networkSystem.Update(gameTime);

            if (_keyboard.IsKeyDown(Keys.Escape)) { Exit(); }

            _prevKeyboard = _keyboard;
            _prevMouseState = _mouseState;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            _graphicsSystem.Draw(_spriteBatch);
            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
