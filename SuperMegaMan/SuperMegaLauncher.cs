﻿using System;

namespace SuperMegaMan
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class SuperMegaLauncher
    {
        public static Game SuperMegaGame;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (SuperMegaGame = new Game())
                SuperMegaGame.Run();
        }
    }
#endif
}
