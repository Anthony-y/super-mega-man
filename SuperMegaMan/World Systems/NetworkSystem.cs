﻿using System;

using System.Threading;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SharpECS;
using Lidgren.Network;

namespace SuperMegaMan.WorldSystems
{
    public class NetworkSystem
        : IGlobalSystem
    {
        public bool IsActive { get; set; } = true;

        private NetPeerConfiguration _config;
        private NetClient _client;

        public NetworkSystem()
        {
            
        }

        public void Update(GameTime gameTime)
        {
            if (_client == null)
            {
                _config = new NetPeerConfiguration("SuperMegaServer");
                _client = new NetClient(_config);
                _client.Start();

                Thread.Sleep(500);

                _client.Connect("localhost", _client.Port);

                Thread.Sleep(500);

            }

            NetIncomingMessage msg;

            while ((msg = _client.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DiscoveryResponse:
                        //_client.Connect(msg.SenderEndPoint);
                        break;
                    case NetIncomingMessageType.Error:
                        Console.WriteLine("Connection error");
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        Console.WriteLine("Status changed");
                        break;
                    case NetIncomingMessageType.Data:
                        Console.WriteLine("Received message");
                        break;
                    case NetIncomingMessageType.DebugMessage:
                        Console.WriteLine("DebugMessage");
                        break;
                    case NetIncomingMessageType.WarningMessage:
                        Console.WriteLine("Warning message");
                        break;
                }
            }
        }

        public void Initialize()
        {
            
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            
        }
    }
}
