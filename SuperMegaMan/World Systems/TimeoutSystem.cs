﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SharpECS;

namespace SuperMegaMan.WorldSystems
{
    public sealed class TimeoutSystem
        : IGlobalSystem
    {
        public bool IsActive { get; set; } = true;

        private Game Game { get; set; }
        public int Time { get; set; }
        private int PrevTime { get; set; }
        private int Timer { get; set; } = 5;

        public TimeoutSystem(Game game)
        {
            Game = game;
        }

        public void Initialize() { }

        public void Update(GameTime gameTime)
        {
            Time = (int)gameTime.TotalGameTime.TotalSeconds;

            if (!IsActive)
            {
                #if DEBUG
                    Console.WriteLine("TimeoutSystem is disabled.");
                #endif
            }
            else
            {
                #if DEBUG
                    if (Time > PrevTime && Time < Timer + 1) { Console.WriteLine("Time: " + Time + "s"); }
                #endif

                if (Time > Timer)
                {
                    Console.WriteLine("It's over!");
                    Game.Exit();
                }
            }

            PrevTime = Time;
        }

        public void Draw(SpriteBatch spriteBatch) { }
    }
}
