﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SharpECS;

using SuperMegaMan.Components;
using SuperMegaMan.Systems;
using SuperMegaMan.WorldSystems;

namespace SuperMegaMan
{
    public sealed class EntityConstructors
    {
        private EntityWorld _entityWorld;

        public EntityConstructors(EntityWorld entityWorld)
        {
            _entityWorld = entityWorld;
        }
        
        public BaseEntity CreatePlayer(Game game)
        {
            var _player = _entityWorld.CreateEntity("Player");

            _player.AddComponent(new GraphicsComponent(_player));
            _player.AddComponent(new TransformComponent(_player, new Vector2(24, 70), new Vector2(1, 1)));
            _player.AddComponent(new BorderCollisionComponent(_player));
            _player.AddComponent(new ColliderComponent(_player));
            _player.AddComponent(new ControllerComponent(_player));

            return _player;
        }

        public BaseEntity CreateNetworkedPlayer(Game game)
        {
            var _networkedPlayer = _entityWorld.CreateEntity("NetworkedPlayer");

            _networkedPlayer.AddComponent(new GraphicsComponent(_networkedPlayer));
            _networkedPlayer.AddComponent(new TransformComponent(_networkedPlayer, new Vector2(24, 70), new Vector2(1, 1)));
            _networkedPlayer.AddComponent(new BorderCollisionComponent(_networkedPlayer));
            _networkedPlayer.AddComponent(new ColliderComponent(_networkedPlayer));

            return _networkedPlayer;
        }

        //public BaseEntity CreateEnemy(Game game)
        //{
            //var _enemy = _entityWorld.CreateEntity("Enemy");

            //_enemy.AddComponent(new GraphicsComponent(_enemy));
            //_enemy.AddComponent(new TransformComponent(_enemy, new Vector2(1, 98), new Vector2(1, 1)));
            //_enemy.AddComponent(new BorderCollisionComponent(_enemy));
            //_enemy.AddComponent(new ColliderComponent(_enemy));

            //return _enemy;
        //}
    }
}
