﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

using SharpECS;
using solidsnake;

using SuperMegaMan.Components;

namespace SuperMegaMan.Systems
{
    public class CollisionSystem
        : EntitySystem<ColliderComponent>
    {
        public CollisionSystem(EntityWorld entityWorld)
            : base(entityWorld)
        {

        }

        public override void Update(GameTime gameTime)
        {
            for (int i = 0; i < CompatibleEntities.Count; i++)
            {
                CompatibleEntities[i].FindComponent<ColliderComponent>().Collider = new Rectangle
                (
                    (int)CompatibleEntities[i].FindComponent<TransformComponent>().Position.X, 
                    (int)CompatibleEntities[i].FindComponent<TransformComponent>().Position.Y, 
                    CompatibleEntities[i].FindComponent<GraphicsComponent>().Texture.Width,
                    CompatibleEntities[i].FindComponent<GraphicsComponent>().Texture.Height
                );

                try
                {
                    if (CompatibleEntities[i].FindComponent<ColliderComponent>().Collider.Intersects(
                    CompatibleEntities[i + 1].FindComponent<ColliderComponent>().Collider))
                    {
                        Console.WriteLine("Collision m8");
                    }
                } catch { }
            }

            base.Update(gameTime);
        }

    }
}
