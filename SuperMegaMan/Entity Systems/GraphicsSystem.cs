﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SharpECS;
using solidsnake;

using SuperMegaMan.Components;

namespace SuperMegaMan.Systems
{
    public sealed class GraphicsSystem
        : EntitySystem<GraphicsComponent>
    {
        public ScriptManager _scriptManager { get; set; }

        public GraphicsSystem(EntityWorld entityWorld)
            : base(entityWorld)
        {
            _scriptManager = new ScriptManager(new Script("Scripts\\Graphics\\system.py"));
        }
        
        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (var i in CompatibleEntities)
            {
                spriteBatch.Draw
                (
                    i.FindComponent<GraphicsComponent>().Texture,
                    i.FindComponent<TransformComponent>().Position,
                    scale: i.FindComponent<TransformComponent>().Scale,
                    color: Color.White
                );
            }

            base.Draw(spriteBatch);
        }
    }
}
