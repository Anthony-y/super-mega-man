﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using SharpECS;

using SuperMegaMan.Components;

namespace SuperMegaMan.Systems
{
    public class ControllerSystem
        : EntitySystem<ControllerComponent>
    {
        public ControllerSystem(EntityWorld entityWorld)
            : base(entityWorld)
        {

        }

        public override void Update(GameTime gameTime)
        {
            foreach (var i in this.CompatibleEntities)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.A))
                {
                    i.FindComponent<TransformComponent>().Position.X -= i.FindComponent<ControllerComponent>().MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }

                if (Keyboard.GetState().IsKeyDown(Keys.D))
                {
                    i.FindComponent<TransformComponent>().Position.X += i.FindComponent<ControllerComponent>().MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }

                if (Keyboard.GetState().IsKeyDown(Keys.W))
                {
                    i.FindComponent<TransformComponent>().Position.Y -= i.FindComponent<ControllerComponent>().MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }

                if (Keyboard.GetState().IsKeyDown(Keys.S))
                {
                    i.FindComponent<TransformComponent>().Position.Y += i.FindComponent<ControllerComponent>().MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }

            }

            base.Update(gameTime);
        }

    }
}
