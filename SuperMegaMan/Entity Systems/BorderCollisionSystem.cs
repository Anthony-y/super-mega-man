﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SharpECS;
using solidsnake;

using SuperMegaMan.Components;

namespace SuperMegaMan.Systems
{
    public sealed class BorderCollisionSystem
        : EntitySystem<BorderCollisionComponent>
    {
        public Vector2 OwnerPosition { get; set; }

        public BorderCollisionSystem(EntityWorld entityWorld)
            : base(entityWorld)
        {
            OwnerPosition = ComponentOwner.FindComponent<TransformComponent>().Position;
        }

        public override void Update(GameTime gameTime)
        {
            float time = (float)gameTime.TotalGameTime.TotalSeconds;

            foreach (var i in CompatibleEntities)
            {
                OwnerPosition = i.FindComponent<TransformComponent>().Position;

                // 32 is the width and height of the player sprite.
                if (OwnerPosition.X <= 0) { i.FindComponent<TransformComponent>().Position = new Vector2(0, OwnerPosition.Y); }
                if (OwnerPosition.X >= 640 - 32) { i.FindComponent<TransformComponent>().Position = new Vector2(640 - 32, OwnerPosition.Y); }

                if (OwnerPosition.Y <= 0) { i.FindComponent<TransformComponent>().Position = new Vector2(OwnerPosition.X, 0); }
                if (OwnerPosition.Y >= 480 - 32) { i.FindComponent<TransformComponent>().Position = new Vector2(OwnerPosition.X, 480 - 32); }
            }

            base.Update(gameTime);
        }

    }
}
