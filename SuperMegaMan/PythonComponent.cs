﻿using SharpECS;

namespace SuperMegaMan
{
    public sealed class PythonComponent 
        : IComponent
    {
        public IEntity Owner { get; set; }
        public string Tag { get; set; }
    }
}
