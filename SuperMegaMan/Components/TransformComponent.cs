﻿using System;

using Microsoft.Xna.Framework;

using SharpECS;
using solidsnake;

namespace SuperMegaMan.Components
{
    public sealed class TransformComponent
        : IComponent
    {
        public IEntity Owner { get; set; }
        public string Tag { get; set; }

        private ScriptManager _scriptManager { get; set; }

        public Vector2 Position;
        public Vector2 Scale;

        public Vector2 Acceleration = new Vector2(50f, 50f);
        public Vector2 Velocity = new Vector2(50f, 50f);

        public int Gravity = 100;

        public bool IsJumped { get; set; } = false;

        public TransformComponent(IEntity owner, Vector2 position, Vector2 scale)
        {
            Owner = owner;
            Position = position;
            Scale = scale;

            _scriptManager = new ScriptManager(new Script("Scripts\\Transform\\component.py"));
            _scriptManager.CompileAndRunFromPath();

            _scriptManager.RegisterClass("TransformComponent");
            _scriptManager.RegPyVar("this", this);

            _scriptManager.CallMethod("main");
        }

        public void IncrementX(string id, float offset)
        {
            if (id.ToLower() == "position")
                Position.X += offset;
            else if (id.ToLower() == "scale")
                Scale.X += offset;
            else if (id.ToLower() == "acceleration")
                Acceleration.X += offset;
        }

        public void IncrementY(string id, float offset)
        {
            if (id.ToLower() == "position")
                Position.Y += offset;
            else if (id.ToLower() == "scale")
                Scale.Y += offset;
            
            else if (id.ToLower() == "acceleration")
                Acceleration.Y += offset;
        }

        public void DecrementX(string id, float offset)
        {
            if (id.ToLower() == "position")
                Position.X -= offset;
            else if (id.ToLower() == "scale")
                Scale.X -= offset;
           
            else if (id.ToLower() == "acceleration")
                Acceleration.X -= offset;
        }

        public void DecrementY(string id, float offset)
        {
            if (id.ToLower() == "position")
                Position.Y -= offset;
            else if (id.ToLower() == "scale")
                Scale.Y -= offset;
           
            else if (id.ToLower() == "acceleration")
                Acceleration.Y -= offset;
        }

        
    }
}
