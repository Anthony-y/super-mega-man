﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SharpECS;
using solidsnake;

namespace SuperMegaMan.Components
{
    public sealed class GraphicsComponent
        : IComponent
    {
        public IEntity Owner { get; set; }
        public string Tag { get; set; }

        public Texture2D Texture { get; set; }

        public GraphicsComponent(IEntity owner, Texture2D texture)
        {
            Owner = owner;
            Texture = texture;
        }

        public GraphicsComponent(IEntity owner)
        {
            Owner = owner;
        }

    }
}
