﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpECS;

namespace SuperMegaMan.Components
{
    public class ControllerComponent
        : IComponent
    {
        public IEntity Owner { get; set; }
        public string Tag { get; set; }

        public float MoveSpeed { get; set; } = 700;

        public ControllerComponent(IEntity owner)
        {
            Tag = "ControllerComponent";
            Owner = owner;
        }
    }
}
