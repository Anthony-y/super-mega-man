﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SharpECS;
using solidsnake;

namespace SuperMegaMan.Components
{
    public sealed class BorderCollisionComponent
        : IComponent
    {
        public IEntity Owner { get; set; }
        public string Tag { get; set; }

        public BorderCollisionComponent(IEntity owner)
        {
            Owner = owner;
        }
    }
}
