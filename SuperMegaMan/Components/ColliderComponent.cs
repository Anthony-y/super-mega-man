﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SharpECS;
using solidsnake;

namespace SuperMegaMan.Components
{
    public class ColliderComponent
        : IComponent
    {
        public IEntity Owner { get; set; }
        public string Tag { get; set; }

        public Rectangle Collider { get; set; }

        public ColliderComponent(IEntity owner)
        {
            Owner = owner;
            Collider = new Rectangle(0, 0, 0, 0);
        }
    }
}
